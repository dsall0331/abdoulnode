 var distance = require('gps-distance');
//
// console.log(result)
var express = require('express')
var bodyParser = require('body-parser')
var app = express()
app.use(bodyParser.json())
 
app.get('/', function (req, res) {
    res.send('hello ')
  })

app.post('/distance', (req, res) =>{
    console.log(req.body);
    var result = distance(req.body.lat1, req.body.lon1, req.body.lat2, req.body.lon2);
    res.status(200).json({
        "distance": result
    });
})
app.listen(3000)
